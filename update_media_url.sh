#!/bin/sh -ex

MEDIA_COMMIT=$(git log -n 1 --pretty=format:%h content/media)
sed -i 's@^media_url: /media@media_url: /media-'${MEDIA_COMMIT}'@' site.yaml
sed -i 's@MEDIA_URL@media-'${MEDIA_COMMIT}'@' nginx.conf
