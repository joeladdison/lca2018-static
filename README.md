# lca2018-static

This projected was generated with [Hyde](http://hyde.github.io/) [v0.8.9](https://github.com/hyde/hyde/releases/tag/v0.8.9)

## Development Environment

    git clone git@gitlab.com:LCA2018/lca2018-static.git
    cd lca2018-static
    virtualenv .
    source bin/activate
    pip install -r requirements.txt

## Running a development server

    cd lca2018-static
    source bin/activate
    hyde gen
    hyde serve

Browse to http://localhost:8080/

## Creating new content

### Metadata
Put the following lines of metadata (altered for your particular page) at the top of the file in order to inherit from the right template.

    ---
    extends: base.j2
    default_block: main
    title: linux.conf.au 2018 - Sydney 22-26 January 2018
    description: linux.conf.au 2018 homepage
    ---

### Markdown

In the body of your page, you can use:

    {% markdown %}
    # Markdown content

    Write whatever you like in normal markdown
    {% endmarkdown %}

## Thanks

This project contains sample code derived from [Hyde](http://hyde.github.io), for which we thank Hyde's various [authors](https://github.com/hyde/hyde/graphs/contributors)
